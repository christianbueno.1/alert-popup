// const openModalButtons = document.querySelectorAll("div> div")
const openModalButtons = document.querySelectorAll('[data-modal-target]')
const closeModalButtons = document.querySelectorAll("[data-close-button]")
const overlay = document.getElementById('overlay')

console.log(openModalButtons.length)
console.log(openModalButtons)
openModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        console.log(button.dataset.modalTarget)
        const modal = document.querySelector(button.dataset.modalTarget)
        console.log('inside forEach')
        console.log(modal)
        openModal(modal)
        console.log(modal)

    })
})
console.log('--- close ---')
console.log(closeModalButtons)
closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.modal')
        console.log(modal)
        closeModal(modal)
        console.log(modal)
    })
})

function openModal(modal) {
    if (modal == null) return
    modal.classList.add('active')
    overlay.classList.add('active')
}
function closeModal(modal) {
    if (modal == null) return
    modal.classList.remove('active')
    overlay.classList.remove('active')
}
overlay.addEventListener('click', () => {
    const modals = document.querySelectorAll('.modal.active')
    modals.forEach(modal => {
        closeModal(modal)
    })
})

console.log('hello')
